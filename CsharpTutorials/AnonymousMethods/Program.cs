﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousMethods
{
    class Program
    {private delegate int PointToAddFunction(int num1, int num2);

        private static void Main(string[] args)
        {
            Stopwatch s = new Stopwatch();
            for (int y = 0; y < 10; y++)
            {
                s.Reset();
                s.Start();
                for (int i = 0; i < 1000; i++)
                {
                    PointToAddFunction p = (a, b) => a + b;
                    int e = p(12, 12);
                }
                s.Stop();
                Console.WriteLine(s.ElapsedTicks.ToString());

            }
        }
    }
}

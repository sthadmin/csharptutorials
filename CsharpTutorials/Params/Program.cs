﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Params
{
    class Program
    {
        private delegate double AddNumbers(params double[] nums);
        static void Main(string[] args)
        {
            AddNumbers add = nb => nb.Sum();

            Console.WriteLine(add(23, 23, 23, 34, 23, 233));
            Console.ReadLine();

        }
    }
}
